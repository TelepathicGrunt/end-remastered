<p>&nbsp;</p>
<p><img src="https://i.imgur.com/oeH3g6F.png" alt="End Remastered Banner" width="1040" height="289" /></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;&nbsp;</span></p>
<p style="text-align: center;"><a title="discord invite" href="https://discord.com/invite/D9cxayDNSP"><img src="https://img.shields.io/discord/781617369285263400?label=Discord&amp;logo=Discord&amp;logoColor=ffffff&amp;style=for-the-badge" alt="Discord" /> &nbsp; &nbsp; </a><a title="Files" href="https://www.curseforge.com/minecraft/mc-mods/endremastered/files"><img src="https://img.shields.io/badge/MC+Version-1.16.3%2B-626EFF?style=for-the-badge&amp;logo=CurseForge" alt="Files" width="196" height="27" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a><a title="Trailer" href="https://www.youtube.com/watch?v=FMwke39lZu8"><img src="https://img.shields.io/badge/YouTube-Trailer-FF2020?style=for-the-badge&amp;logo=YouTube&amp;logoColor=FF2020" alt="Youtube Trailer" width="180" height="28" /></a></p>
<p>&nbsp;</p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;"><strong>End Remastered aims to make your Minecraft experience more challenging by emphasizing the adventurous side of the game. In brief, 11 new Ender Eyes are added along with a giant castle and a challenging dungeon which replace Vanilla Strongholds. We currently support 1.16.3, 1.16.4 and 1.16.5 (We have a beta release for 1.16.1, but we would not suggest using it, since it lacks so many features due to incompatibility.)<br /></strong></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;<br /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">Exploration is the main focus of End Remastered, "It's not the destination, but the journey." In order to go to the End and beat the Ender Dragon, you will have to find the 11 custom eyes by exploring vanilla structures and fighting vanilla bosses. Once you have the eyes, you have the choice to follow them until you reach an End Gate or to trade with a Cartographer to obtain a map to the End Castle. </span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">After reaching one of these structures, you will have to find the hidden portal room, place your eyes in the portal frames and, finally, activate it by using a powered core on the End Creator (the grey frame)<br /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 24px;">&nbsp;</span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;"><span style="font-size: 24px;"><strong>Our Partners:</strong></span></span></p>
<p><span style="font-size: 14px;">Thanks to Apex Hosting for partnering with End Remastered!<strong><br />Are you looking for a place to play Minecraft with your friends? </strong>Well look no further because <span style="color: #000080;"><a style="color: #000080;" href="https://billing.apexminecrafthosting.com/aff.php?aff=4481"><span style="text-decoration: underline;"><span style="color: #ff6600; text-decoration: underline;">Apex Hosting</span></span></a></span> got you covered! With their great and easy to use Control Panel and their multiple servers spread across the entire globe, you can have your own Minecraft server up and running in about 5 minutes. Use this link: <span style="text-decoration: underline; color: #ff6600;">https://billing.apexminecrafthosting.com/aff.php?aff=4481</span> or click on the image below to get started now!<br /></span></p>
<p><span style="font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 14px;"><a href="https://billing.apexminecrafthosting.com/aff.php?aff=4481"><img src="https://i.imgur.com/wXSNHmv.png" width="645" height="161" border="0" /></a></span></p>
<p>&nbsp;<span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<p>&nbsp;</p>
<p><a href="https://endremastered.fandom.com/wiki/The_Eyes"><img src="https://i.imgur.com/PuRYpVn.png" alt="The Eyes" width="1042" height="174" /></a><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><br /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">The Eyes are a really important part of End Remastered, just like Vanilla Ender Eyes, they allow you to activate the portal to the End. However, unlike Vanilla Ender Eyes, they are not dropped by Endermen but must be found by exploring, fighting and mining. For more info, click the spoiler tag below or learn more about them by clicking on the banner above this paragraph.</span></p>
<p>&nbsp;</p>
<div class="spoiler">
<p><span style="font-size: 14px;"><strong>1. Old Eye - Found in Desert Pyramids</strong> </span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">Legends say this eye once belonged to one of the great Sandworms</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>2. Nether Eye - Found in Nether Fortresses</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">It was lost by a demon in the battle between the Aether and the Nether</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>3. Cold Eye - Found in Igloos</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">One of the last artifacts left behind by the Iceologers before they all suddenly vanished from the world</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>4. Rogue Eye - Found in Jungle Pyramids</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">This eye is believed to have given our ancestors the knowledge of Redstone</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>5. Black Eye - Found in Buried Chests</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">Pirates say it gave sight to the legendary Black Pearl</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>6. Magical Eye - Dropped by Evokers<br /></strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">Without this eye, the Evoker has no power</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>7. Lost Eye - Found in Mineshafts</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">It is told that it was forged by the first blacksmiths of time</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>8. Corrupted Eye - Found in Pillager Outposts</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">The eye of a greedy king that faded in solitude. Legend says it will bring infinite fortune to its owner</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>9. Wither Eye - Dropped by The Wither</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">Has witnessed destruction. Maybe it also witnessed your stuff disappear</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>10. Guardian Eye - Get it by Killing Elder Guardian</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">It has seen many warriors drown into the depths of the sea</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><strong>11. End Crystal Eye - Get it by crafting it using the End Crystal Fragment, a new ore.</strong></span></p>
<p style="padding-left: 30px;"><span style="font-size: 14px;">It is believed to hold a fragment of the Enderdragon Soul</span></p>
</div>
<p>&nbsp;</p>
<p>&nbsp;<span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong>&nbsp;</strong></span></p>
<p><span style="font-size: 14px;"><a href="https://endremastered.fandom.com/wiki/End_Castle"><span style="font-family: trebuchet ms, geneva, sans-serif;"><strong><img src="https://i.imgur.com/0yepcQM.png" alt="End Castle" width="1030" height="172" /></strong></span></a></span></p>
<p>&nbsp;</p>
<p><span style="font-size: 14px;">The End Castle can be found using a Map (However this can be changed in the Config File) that you can obtain by trading with a cartographer. It is a beautiful and massive structure, that focuses on appearance and atmosphere. Click the spoiler tag below to see pictures of this new structure or learn more about it by clicking on the banner above this paragraph.<strong><br /></strong></span></p>
<p>&nbsp;</p>
<div class="spoiler">
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img src="https://media.discordapp.net/attachments/473982781157015557/780172158319591475/2020-11-22_13.20.40.png?width=789&amp;height=493" width="705" height="441" /></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img src="https://media.discordapp.net/attachments/473982781157015557/780172155442429982/2020-11-22_13.20.24.png?width=789&amp;height=493" width="705" height="441" /></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img src="https://cdn.discordapp.com/attachments/473982781157015557/780172151775821832/2020-11-22_13.19.34.png" width="705" height="441" /></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img src="https://cdn.discordapp.com/attachments/473982781157015557/780171827308920841/2020-11-22_13.png" width="705" height="441" /></strong></span></p>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><a href="https://endremastered.fandom.com/wiki/The_Dungeon"><img src="https://i.imgur.com/XW3BeJj.png" alt="End Gate" width="1040" height="174" /></a><br /></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">The End Gate is an extensive dungeon that focuses on exploration, traps and challenges. In order to find the custom portal hidden inside it, you will have to use your creativity to solve 3 difficult puzzles. All rooms are generated randomly, meaning that every End Gate is different, and even the puzzles change from one to the other. It can be found using the Custom Eyes (This can also be changed in the Config File) just like vanilla strongholds. Click the spoiler tag below to see pictures of this new structure or learn more about it by clicking on the banner above this paragraph.</span></p>
<p>&nbsp;</p>
<div class="spoiler">
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img src="https://i.imgur.com/8Uqo62n.png" alt="End Fortress" width="748" height="421" /><br /></strong></span></p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img class="transparent" src="https://i.imgur.com/u5G9P4w.png" alt="https://i.imgur.com/u5G9P4w.png" width="750" height="422" /></strong></span></p>
<p>&nbsp;</p>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;"><strong><img src="https://i.imgur.com/y8Rnmrg.png" alt="https://i.imgur.com/y8Rnmrg.png" width="749" height="422" /></strong></span></p>
</div>
<p><span style="font-family: trebuchet ms, geneva, sans-serif; font-size: 14px;">&nbsp;</span><span style="font-size: 14px; color: #ff00ff; font-family: trebuchet ms, geneva, sans-serif;" data-darkreader-inline-color=""><strong>&nbsp;</strong></span></p>
<p>&nbsp;<img src="https://i.imgur.com/31tkOM7.png" alt="The Items" width="1042" height="174" /></p>
<p>&nbsp;</p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">End Remastered also adds some items to the game such as new gear and new tools (and a new ore). Click the spoiler button to see various crafting recipes for the custom Items we added to the game.<strong><br /></strong></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;</span></p>
<div class="spoiler">
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;"><strong>The Core:</strong><br /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;"><img class="transparent" src="https://i.imgur.com/l5vmjEM.png" alt="Core" width="256" height="122" /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;"><strong>The&nbsp; Powered Core:</strong><br /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">(Used to activate the portal, hence why there's only 11 eyes)</span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;"><img class="transparent" src="https://i.imgur.com/g1lj2lN.png" alt="Powered Core" width="257" height="122" /></span></p>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 14px;">&nbsp;</span></p>
<p><span style="font-size: 14px;"><strong>The End Crystal Eye:</strong></span><br /><span style="font-size: 14px;"><img class="transparent" src="https://i.imgur.com/So24esj.png" alt="End Crystal eye Craft" width="257" height="124" /></span></p>
</div>
<p><span style="font-family: helvetica, arial, sans-serif; font-size: 18px;">&nbsp;</span><span style="font-size: 18px; font-family: helvetica, arial, sans-serif;"><br /></span></p>
<p>&nbsp;</p>
<p><span style="font-size: 18px;"><em><strong>Support:</strong></em></span></p>
<p><span style="font-size: 14px;">We always try to be aware of issues in End Remastered, if you experience a bug while playing our mod please report it in the comments or the "Issues" section to help us keep the experience fun and enjoyable for all players.<strong><br /></strong></span></p>
<p><span style="font-size: 14px;">&nbsp;</span><span style="font-size: 14px;"><br /></span></p>
<p><span style="font-size: 14px;"><span style="color: #3366ff;" data-darkreader-inline-color=""><strong>&nbsp;<a style="color: #3366ff;" href="https://discord.gg/D9cxayDNSP" data-darkreader-inline-color="">CLICK HERE</a> </strong></span><strong>to&nbsp;</strong><strong>join our discord server to speak, chat, report problems o</strong><strong>r play with us :)</strong></span></p>
