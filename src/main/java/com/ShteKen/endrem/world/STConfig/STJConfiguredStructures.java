package com.ShteKen.endrem.world.STConfig;

import com.ShteKen.endrem.EndRemastered;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.WorldGenRegistries;
import net.minecraft.world.gen.FlatGenerationSettings;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.StructureFeature;
public class STJConfiguredStructures {

    public static StructureFeature<?, ?> CONFIGURED_END_GATE = STJStructures.END_GATE.get().withConfiguration(IFeatureConfig.NO_FEATURE_CONFIG);

    public static void registerConfiguredStructures() {
        Registry<StructureFeature<?, ?>> registry = WorldGenRegistries.CONFIGURED_STRUCTURE_FEATURE;
        Registry.register(registry, new ResourceLocation(EndRemastered.MOD_ID, "configured_end_gate"), CONFIGURED_END_GATE);

        FlatGenerationSettings.STRUCTURES.put(STJStructures.END_GATE.get(), CONFIGURED_END_GATE);
    }
}
