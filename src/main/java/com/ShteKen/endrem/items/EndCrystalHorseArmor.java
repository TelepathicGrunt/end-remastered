package com.ShteKen.endrem.items;

import com.ShteKen.endrem.EndRemastered;
import net.minecraft.item.HorseArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;

public class EndCrystalHorseArmor extends HorseArmorItem {

    public EndCrystalHorseArmor() {
        super(13, new ResourceLocation(EndRemastered.MOD_ID, "textures/entity/horse/armor/horse_armor_end_crystal.png"),
                (new Item.Properties()).maxStackSize(1).group(EndRemastered.TAB)); }
    @Override
    public int getArmorValue() {
        return 13;
    }
}
